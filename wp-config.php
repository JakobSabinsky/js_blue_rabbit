<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jakob_blue_rabbit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';DheqOe0c+.}aOaApVVh%hcPlN57y[aLAfaUsob&f%2-V;A?&[M5sQ@)uzYSkpX3');
define('SECURE_AUTH_KEY',  '$g*J`_= Jrvq{m~gWobd}TAPI)lx;8[`Gw5%hRZE?E`Nf+mAIQ(;l[T/qfX*eX/:');
define('LOGGED_IN_KEY',    'K}E#]QS|R8%NmrPQk?^j&js[-dRUQ?hiJq=_r.j~`QXzcF0yppvy*72(<FWfKm/-');
define('NONCE_KEY',        'S5*0;H1Oa` w<!)p)W{8t60nbcqGy1HdD7HPLm^F)ys0$Q-ftZhm@L8l.+miwhLD');
define('AUTH_SALT',        'VsQUgFH?S~eUgHwG&!8CFUwBKZakF$+/D l%$F+y=[0Lb<ZpJOX{k2z% 810v@wm');
define('SECURE_AUTH_SALT', 'yOw@ba@]s-Z3(&f8yT)y<XUnpY&jE:~>%^[fA5=f*yz>![rf$Gt|f@rYRcm]h_&S');
define('LOGGED_IN_SALT',   '{&pNhqgGZ6~z(ONvB?O=Gg{]nLXX_Us^616 ^tvK4VG.(tx<fh-c N<oZw@K|V%N');
define('NONCE_SALT',       'O0[`^UjnS/o:%%2WcCSk9SHXaNRs/<`V2Exr%oa>Z/QWDhW/S{J$7^B7AW:Q>JN0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpdb2_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
